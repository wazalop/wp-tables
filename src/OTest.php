<?php

namespace Openscop\OTest;

use Openscop\OTest\services\content\BackContent;
use Openscop\OTest\services\content\FrontContent;
use Openscop\OTest\services\install\Activator;
use Openscop\OTest\services\install\Deactivator;
use Openscop\OTest\services\install\I18n;
use Openscop\OTest\services\install\Loader;
use Openscop\OTest\services\install\Uninstall;

class OTest
{
    use Activator, Deactivator, Uninstall;
    const PLUGIN_NAME = 'O-test';
    const PLUGIN_VERSION = '1.0.0';
    const TEXT_DOMAIN = 'openscop_otest';
	const PLUGIN_DIR = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'o-test' . DIRECTORY_SEPARATOR;

    public static string $ASSETS;
    public static string $TEMPLATE_DIR;

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     */
    protected Loader $loader;

    public function __construct() {
        self::$ASSETS = self::PLUGIN_DIR . 'assets' . DIRECTORY_SEPARATOR;
        self::$TEMPLATE_DIR = self::PLUGIN_DIR . 'templates' . DIRECTORY_SEPARATOR;

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();
    }

    private function load_dependencies(): void
    {
        $this->loader = new Loader();
    }

    private function set_locale(): void
    {
        $plugin_i18n = new I18n();

        $this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
    }

    /**
     * Register all the hooks related to the admin area functionality
     * of the plugin.
     */
    private function define_admin_hooks(): void
    {
        $plugin_admin = new BackContent();

        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
        $this->loader->add_action( 'admin_menu', $plugin_admin, 'render_menu' );
    }

    /**
     * Register all the hooks related to the public-facing functionality
     * of the plugin.
     */
    private function define_public_hooks(): void
    {
        $plugin_public = new FrontContent();

        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
    }

    /**
     * Run the loader to execute all the hooks with WordPress.
     */
    public function run(): void
    {
        $this->loader->run();
    }

}