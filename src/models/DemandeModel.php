<?php

namespace Openscop\OTest\models;

class DemandeModel {
	use TableInitialisationTrait;
	static string $BASE_TABLE_NAME = 'o_demandes';
	static string $TABLE_VERSION = "1.0";
	static string $TABLE_VERSION_OPTION = 'table_o_demandes_version';
    static array $COLUMNS = [
		"id mediumint(9) NOT NULL AUTO_INCREMENT",
	    "status enum('ACCEPTED', 'REFUSED')",
	    "name varchar(25) NOT NULL",
	    "PRIMARY KEY  (id)"
    ];

	public static function getAccepted( array|string $orderby, array|string $order ): array|null {
		global $wpdb;
		$query = "SELECT id, name, status FROM " . self::getTableName() . " ORDER BY $orderby $order";
		return $wpdb->get_results( $query, ARRAY_A  );
	}

}