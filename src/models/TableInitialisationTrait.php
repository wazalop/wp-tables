<?php

namespace Openscop\OTest\models;

use Openscop\OTest\services\Utils;

trait TableInitialisationTrait {
	static function getTableName(): string {
		global $wpdb;
		return $wpdb->prefix . self::$BASE_TABLE_NAME;
	}

	static function createTable(): void {
		global $wpdb;

		$sql = "CREATE TABLE " . self::getTableName() . " ( ";

		$max = sizeof(self::$COLUMNS);
		for ($i = 0; $i < $max; $i++) {
			$sql .= self::$COLUMNS[$i];
			if($i+1 < $max)
				$sql .= ",";
		}

		$sql .= ");";

		$wpdb->query($sql);

		Utils::updateOption(self::$TABLE_VERSION_OPTION, self::$TABLE_VERSION);
	}

	static function deleteTable(): void {
		global $wpdb;

		$sql = "DROP TABLE " . self::getTableName() . ";";

		$wpdb->query($sql);

		Utils::deleteOption(self::$TABLE_VERSION_OPTION, self::$TABLE_VERSION);
	}
}