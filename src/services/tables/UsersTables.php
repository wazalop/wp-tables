<?php

namespace Openscop\OTest\services\tables;

use JetBrains\PhpStorm\NoReturn;
use Openscop\OTest\models\DemandeModel;
use Openscop\OTest\OTest;
use WP_List_Table;

class UsersTables extends WP_List_Table {

	function get_columns(): array {
		return [
			'cb'     => '<input type="checkbox" />', // to display the checkbox.
			'name'   => __( 'Name', OTest::TEXT_DOMAIN ),
			'status' => __( 'Status', OTest::TEXT_DOMAIN ),
		];
	}

	public function get_sortable_columns(): array {
		return [
			'name'   => [ 'name', true ],
			'status' => [ 'status', true ],
		];
	}

	function prepare_items() {
		// check if a search was performed.
		$search_key = isset( $_REQUEST['s'] ) ? wp_unslash( trim( $_REQUEST['s'] ) ) : '';

		$this->_column_headers    = $this->get_column_info();
		$this->_column_headers[0] = $this->get_columns();

		// check and process any actions such as bulk actions.
		$this->handle_table_actions();

		// fetch table data
		$table_data = $this->fetch_table_data();
		// filter the data in case of a search.
		if ( $search_key ) {
			$table_data = $this->filter_table_data( $table_data, $search_key );
		}

		// required for pagination
		$users_per_page = $this->get_items_per_page( 'users_per_page' );
		$table_page     = $this->get_pagenum();

		// provide the ordered data to the List Table.
		// we need to manually slice the data based on the current pagination.
		$this->items = array_slice( $table_data, ( ( $table_page - 1 ) * $users_per_page ), $users_per_page );

		// set the pagination arguments
		$total_users = count( $table_data );
		$this->set_pagination_args( array(
			'total_items' => $total_users,
			'per_page'    => $users_per_page,
			'total_pages' => ceil( $total_users / $users_per_page )
		) );
	}

	/**
	 * Text displayed when no user data is available
	 */
	public function no_items(): void {
		_e( 'No users available.', OTest::TEXT_DOMAIN );
	}

	/*
	 * Fetch table data from the WordPress database.
	 */
	public function fetch_table_data(): array|object|null {
		$orderby = ( isset( $_GET['orderby'] ) ) ? esc_sql( $_GET['orderby'] ) : 'id';
		$order   = ( isset( $_GET['order'] ) ) ? esc_sql( $_GET['order'] ) : 'ASC';

		return DemandeModel::getAccepted( $orderby, $order );
	}

	/*
	 * Filter the table data based on the user search key
	 */
	public function filter_table_data( $table_data, $search_key ): array {
		return array_values( array_filter( $table_data, function ( $row ) use ( $search_key ) {
			foreach ( $row as $row_val ) {
				if ( stripos( $row_val, $search_key ) !== false ) {
					return true;
				}
			}
			return false;
		} ) );
	}

	/**
	 * Render a column when no column specific method exists.
	 */
	public function column_default( $item, $column_name ): mixed {
		return $item[ $column_name ];
	}

	/**
	 * Get value for checkbox column.
	 *
	 * The special 'cb' column
	 */
	protected function column_cb( $item ): string {
		$id = $item['id'];

		return '<label class="screen-reader-text" for="data_' . $id . '">' . sprintf( __( 'Select %s' ), $item['status'] ) . '</label>'
		       . "<input type='checkbox' name='data[]' id='data_{$id}' value='{$id}' />";
	}

	protected function column_name( $item ): string {
		$actions = array(
			'save'   => sprintf( '<a href="?page=%s&action=%s&element=%s">' . __( 'Accepter', OTest::TEXT_DOMAIN ) . '</a>', $_REQUEST['page'], 'save', $item['id'] ),
			'delete' => sprintf( '<a href="?page=%s&action=%s&element=%s">' . __( 'Refuser', OTest::TEXT_DOMAIN ) . '</a>', $_REQUEST['page'], 'delete', $item['id'] ),
		);

		return sprintf( '%1$s %2$s', $item['name'], $this->row_actions( $actions ) );
	}

	/**
	 * Returns an associative array containing the bulk action
	 */
	public function get_bulk_actions(): array {
		return [
			'bulk-save'   => __( 'Accepter', OTest::TEXT_DOMAIN ),
			'bulk-delete' => __( 'Refuser', OTest::TEXT_DOMAIN ),
		];
	}

	/**
	 * Process actions triggered by the user
	 */
	public function handle_table_actions() {
		// check for individual row actions
		$the_table_action = $this->current_action();
		if(in_array($the_table_action, ['save', 'delete']) && isset($_REQUEST['element'])) {
			if($the_table_action === 'save') $this->actionAcceptData([$_REQUEST['element']]);
			if($the_table_action === 'delete') $this->actionCancelData([$_REQUEST['element']]);
		} else if(str_starts_with($the_table_action, 'bulk-') && isset($_REQUEST['data'])) {
			$nonce = wp_unslash( $_REQUEST['_wpnonce'] );
			// verify the nonce.
			if ( ! wp_verify_nonce( $nonce, 'bulk-' . $this->_args['plural']  ) ) {
				$this->invalid_nonce_redirect();
			} else {
				if($the_table_action === 'bulk-save') $this->actionAcceptData($_REQUEST['data']);
				if($the_table_action === 'bulk-delete') $this->actionCancelData($_REQUEST['data']);
			}
		}
	}

	function actionAcceptData(array $data): void {
		foreach ($data as $entry) {
			// todo : add entry with accepted status
		}
	}

	function actionCancelData(array $data): void {
		foreach ($data as $entry) {
			// todo : add entry with cancelled status
		}
	}

	/**
	 * Die when the nonce check fails.
	 */
	#[NoReturn] public function invalid_nonce_redirect(): void {
		wp_die( __( 'Invalid Nonce', OTest::TEXT_DOMAIN ),
			__( 'Error', OTest::TEXT_DOMAIN ),
			[
				'response'  => 403,
				'back_link' => esc_url( add_query_arg( [ 'page' => wp_unslash( $_REQUEST['page'] ) ] ) ),
			]
		);
	}

}