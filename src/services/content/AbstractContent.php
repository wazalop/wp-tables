<?php

namespace Openscop\OTest\services\content;

abstract class AbstractContent
{
    /**
     * Register the stylesheets for this side of the site.
     */
    abstract public function enqueue_styles(): void;

    /**
     * Register the JavaScript for this side of the site.
     */
    abstract public function enqueue_scripts(): void;
}