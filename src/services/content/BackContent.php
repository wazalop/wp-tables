<?php

namespace Openscop\OTest\services\content;

use Openscop\OTest\OTest;
use Openscop\OTest\services\tables\UsersTables;

class BackContent extends AbstractContent
{
	private UsersTables $table;

	public function enqueue_styles(): void
    {
        // TODO: Implement enqueue_styles() method.
    }

    public function enqueue_scripts(): void
    {
        // TODO: Implement enqueue_scripts() method.
    }

    public function render_menu() {
	    $page_hook = add_menu_page(
			'Manuels',
			'Manuels',
			'publish_posts',
			"OTest",
			[$this, 'render_list_todo']
        );
        add_submenu_page(
			"OTest",
			'Accepté',
			'Accepté',
			'edit_posts',
			'OTest-accept',
			[$this, 'render_list_accepted']
        );
        add_submenu_page(
			"OTest",
			'Refusé',
			'Refusé',
			'edit_posts',
			'OTest-cancel',
			[$this, 'render_list_cancelled']
        );
		add_action( 'load-'.$page_hook, [$this, 'tables_screen_options'] );
    }

	public function tables_screen_options(): void {
		$perPageOptions = [
			'label'		=>	__( 'Users Per Page', OTest::TEXT_DOMAIN ),
			'default'	=>	5,
			'option'	=>	'users_per_page'
		];
		add_screen_option( 'per_page', $perPageOptions );

		$table = new UsersTables();
	}

    public function render_list_todo(): void
    {
        $this->table = new UsersTables();
		$this->table->prepare_items();
	    include_once( OTest::$TEMPLATE_DIR . 'back' . DIRECTORY_SEPARATOR . 'users_tables.php' );
    }

    public function render_list_accepted(): void
    {
	    $table = new UsersTables();
	    $table->prepare_items();
	    $table->display();
    }

    public function render_list_cancelled(): void
    {
	    $table = new UsersTables();
	    $table->prepare_items();
	    $table->display();
    }
}