<?php

namespace Openscop\OTest\services;

class Utils {

	static function debug(mixed $data, $die = true): void {
		echo '<pre>';
		print_r($data);
		echo '</pre>';
		if($die) die;
	}

	static function updateOption(string $key, mixed $value, int $site_id = null): bool {
		if(is_multisite()) {
			return update_network_option($site_id, $key, $value);
		} else {
			return update_option( $key, $value );
		}
	}

	static function getOptions(string $key, mixed $default, int $site_id = null): mixed {
		if(is_multisite()) {
			return get_network_option($site_id, $key, $default);
		} else {
			return get_option( $key, $default );
		}
	}

	static function deleteOption( string $key, int $site_id = null ): bool {
		if(is_multisite()) {
			return delete_network_option($site_id, $key);
		} else {
			return delete_option( $key );
		}
	}
}