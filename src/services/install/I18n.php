<?php

namespace Openscop\OTest\services\install;

use Openscop\OTest\OTest;

class I18n
{
    /**
     * Load the plugin text domain for translation.
     */
    public function load_plugin_textdomain(): void
    {
        load_plugin_textdomain(
            OTest::PLUGIN_NAME,
            false,
            dirname(plugin_basename(__FILE__), 2) . '/languages/'
        );
    }
}