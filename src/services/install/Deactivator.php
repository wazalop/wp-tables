<?php

namespace Openscop\OTest\services\install;

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @package    OTest
 * @subpackage Openscop/Otest/services
 */
trait Deactivator
{
    public static function deactivate(): void
    {
    }
}