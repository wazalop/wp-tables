<?php

namespace Openscop\OTest\services\install;

use Openscop\OTest\models\DemandeModel;

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @package    OTest
 * @subpackage Openscop/Otest/services
 */
trait Activator
{
    public static function activate(): void {
		DemandeModel::createTable();
    }
}