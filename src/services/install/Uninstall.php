<?php

namespace Openscop\OTest\services\install;

use Openscop\OTest\models\DemandeModel;

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @package    OTest
 * @subpackage Openscop/Otest/services
 */
trait Uninstall
{
    public static function uninstall(): void
    {
		DemandeModel::deleteTable();
    }
}