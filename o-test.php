<?php

/*
Plugin Name: O Test
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: A brief description of the Plugin.
Version: 1.0
Author: wazalop
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/


// If this file is called directly, abort.
use Openscop\OTest\OTest;

if ( ! defined( 'WPINC' ) ) {
    die;
}

require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';


/**
 * The code that runs during plugin activation.
 */
function activate_o_test(): void
{
    OTest::activate();
}

/**
 * The code that runs during plugin deactivation.
 */
function deactivate_o_test(): void
{
    OTest::deactivate();
}

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 */
function run_o_test(): void
{

    $plugin = new OTest();
    $plugin->run();

}


register_activation_hook( __FILE__, 'activate_o_test' );
register_deactivation_hook( __FILE__, 'deactivate_o_test' );

run_o_test();