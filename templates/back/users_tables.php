<?php
use Openscop\OTest\OTest;

?>
<div class="wrap">
    <h2><?php _e( 'WP List Table Demo', OTest::TEXT_DOMAIN); ?></h2>
    <div id="nds-wp-list-table-demo">
        <div id="nds-post-body">
            <form id="nds-user-list-form" method="get">
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
				<?php
				$this->table->search_box( __( 'Find', OTest::TEXT_DOMAIN ), 'openscop-search');
				$this->table->display();
				?>
            </form>
        </div>
    </div>
</div>
